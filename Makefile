START=cd build
BUILD_AND_RUN=cmake --build . && ./test
END=cd ..

release:
	$(START) && cmake -DCMAKE_BUILD_TYPE=Release .. && $(BUILD_AND_RUN) && $(END) 

debug:
	$(START) && cmake -DCMAKE_BUILD_TYPE=Debug .. && $(BUILD_AND_RUN) && $(END)

clean:
	rm -rf build/CMakeCache.txt build/CMakeFiles \
		   build/cmake_install.cmake build/_deps \
		   build/Makefile build/test