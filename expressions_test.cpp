/* Copyright (C) INA - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Author: ObaniGemini <robin@pi-et-ro.net>
 * Created: 2023-07-28
 */

#include <catch2/catch_all.hpp>
#include <cmath>
#include <iostream>
#include <string>

#include "expressions.h"

using T = float;

using Catch::Matchers::IsNaN;
using Catch::Matchers::WithinAbs;

constexpr int SIZE = 32;

template <container A>
static void fillWithRandom(A &a) {
  using VT = typename A::value_type;
  for (VT &i : a) i = VT(GENERATE(take(1, random(1, 100))));
}

TEST_CASE("Scalar operations") {
  const float START = GENERATE(1, 2, 3, 13);
  Array<T, SIZE> a;
  for (T &v : a) v = T(START);

  SECTION("a = 0_f") {
    a = 0.f;
    for (T v : a) REQUIRE_THAT(v, WithinAbs(0.f, 1e-4));
  }

  SECTION("a += 1_f") {
    a += 1.f;
    for (T v : a) REQUIRE_THAT(v, WithinAbs(START + 1.f, 1e-4));
  }

  SECTION("a -= 2_f") {
    a -= 2.f;
    for (T v : a) REQUIRE_THAT(v, WithinAbs(START - 2.f, 1e-4));
  }

  SECTION("a *= 2_f") {
    a *= 2.f;
    for (T v : a) REQUIRE_THAT(v, WithinAbs(START * 2.f, 1e-4));
  }

  SECTION("a /= 2_f") {
    a /= 2.f;
    for (T v : a) REQUIRE_THAT(v, WithinAbs(START / 2.f, 1e-4));
  }
}

TEST_CASE("In-place operations") {
  const auto values = GENERATE(1.f, 2.f, 3.f, 42.33333f, 12345.f);
  const float START = values;

  Array<T, SIZE> a;
  for (T &v : a) v = T(START);

  SECTION("a += a") {
    a += a;
    for (T v : a) REQUIRE_THAT(v, WithinAbs(START + START, 1e-4));
  }

  SECTION("a *= a") {
    a *= a;
    for (T v : a) REQUIRE_THAT(v, WithinAbs(START * START, 1e-4));
  }

  SECTION("a /= a") {
    a /= a;
    for (T v : a) REQUIRE_THAT(v, WithinAbs(START / START, 1e-4));
  }

  SECTION("a -= a") {
    a -= a;
    for (T v : a) REQUIRE_THAT(v, WithinAbs(START - START, 1e-4));
  }

  SECTION("++a") {
    ++a;
    for (T v : a) REQUIRE_THAT(v, WithinAbs(START + 1, 1e-4));
  }

  SECTION("++(++a)") {
    ++(++a);
    for (T v : a) REQUIRE_THAT(v, WithinAbs(START + 2, 1e-4));
  }

  SECTION("--a") {
    --a;
    for (T v : a) REQUIRE_THAT(v, WithinAbs(START - 1, 1e-4));
  }

  SECTION("--(--a)") {
    --(--a);
    for (T v : a) REQUIRE_THAT(v, WithinAbs(START - 2, 1e-4));
  }

  SECTION("++(--(++a))") {
    ++(--(++a));
    for (T v : a) REQUIRE_THAT(v, WithinAbs(START + 1, 1e-4));
  }

  SECTION("check: a = b = c") {
    Array<T, SIZE> b, c, d;
    fillWithRandom(a);
    fillWithRandom(b);
    fillWithRandom(c);
    fillWithRandom(d);

    for (int i = 0; i < a.size(); i++) { c[i] = a[i]; }
    d = b = a;
    for (int i = 0; i < b.size(); i++) {
      REQUIRE_THAT(b[i], WithinAbs(c[i], 1e-4));
      REQUIRE_THAT(c[i], WithinAbs(d[i], 1e-4));
    }
  }
}

TEST_CASE("Expression Iterators") {
  Array<T, SIZE> a, b;
  fillWithRandom(a);
  fillWithRandom(b);
  int i = 0;

  SECTION("Iterate on 'a + b * a'") {
    for (T k : a + b * a) {
      REQUIRE_THAT(k, WithinAbs((a[i] + b[i] * a[i]), 1e-4));
      i++;
    }
  }

  SECTION("Iterate on stored 'a + b * a'") {
    auto e = a + b * a;
    for (T k : e) {
      REQUIRE_THAT(k, WithinAbs((a[i] + b[i] * a[i]), 1e-4));
      i++;
    }
  }

  SECTION("Iterate on stored 'b * a' + a") {
    auto e = b * a;
    for (T k : a + e) {
      REQUIRE_THAT(k, WithinAbs((a[i] + b[i] * a[i]), 1e-4));
      i++;
    }
  }

  SECTION("Iterate on a++") {
    for (int i = 0; i < a.size(); i++) b[i] = a[i];

    for (T k : a++) REQUIRE_THAT(k, WithinAbs(b[i++], 1e-4));

    i = 0;
    for (T v : a) REQUIRE_THAT(v, WithinAbs(b[i++] + 1, 1e-4));
  }

  SECTION("Iterate on a--") {
    for (int i = 0; i < a.size(); i++) b[i] = a[i];

    for (T k : a--) REQUIRE_THAT(k, WithinAbs(b[i++], 1e-4));

    i = 0;
    for (T v : a) REQUIRE_THAT(v, WithinAbs(b[i++] - 1, 1e-4));
  }
}

TEST_CASE("Unary expressions") {
  const auto values = GENERATE(1.f, 2.f, 3.f, 42.33333f, 12345.f);
  const float START = values;

  Array<T, SIZE> a;
  for (T &v : a) v = T(START);

  SECTION("a++") {
    a = a++;
    for (T v : a) REQUIRE_THAT(v, WithinAbs(START, 1e-4));
  }

  SECTION("a--") {
    a = a--;
    for (T v : a) REQUIRE_THAT(v, WithinAbs(START, 1e-4));
  }

  SECTION("a = -a") {
    a = -a;
    for (T v : a) REQUIRE_THAT(v, WithinAbs(-START, 1e-4));
  }

  SECTION("a = abs(-a)") {
    a = abs(-a);
    for (T v : a) REQUIRE_THAT(v, WithinAbs(START, 1e-4));
  }
}

TEST_CASE("Forward") {
  const int fw = GENERATE(1, 3, 7, 13);
  const int forwardSize = SIZE + fw;

  Vector<T> a(forwardSize);
  Vector<T> b(forwardSize);
  Vector<T> c(SIZE);
  Vector<T> d(SIZE);

  fillWithRandom(a);
  fillWithRandom(b);
  c.clear();
  d.clear();

  SECTION("c = forward(a, x)") {
    for (int i = fw; i != forwardSize; ++i) c[i - fw] = a[i];
    d = forward(a, fw);

    for (int i = 0; i < c.size(); i++)  REQUIRE_THAT(c[i], WithinAbs(d[i], 1e-4));
  }

  SECTION("c = forward(a + b, x)") {
    for (int i = fw; i != forwardSize; ++i) c[i - fw] = a[i] + b[i];
    d = forward(a + b, fw);

    for (int i = 0; i < c.size(); i++)  REQUIRE_THAT(c[i], WithinAbs(d[i], 1e-4));
  }
}

TEST_CASE("Cast") {
  const auto values = GENERATE(1, 2, 3, 43);
  const int START = values;

  Array<T, SIZE> a;
  Array<int, SIZE> b;
  for (int &vb : b) vb = START;

  SECTION("a = cast<T>(b)") {
    a = cast<T>(b);
    for (T v : a) REQUIRE_THAT(v, WithinAbs(START, 1e-4));
  }

  SECTION("a = cast<T>(b + b)") {
    a = cast<T>(b + b);
    for (T v : a) REQUIRE_THAT(v, WithinAbs(START + START, 1e-4));
  }

  SECTION("a = cast<T>(b + cube(b))") {
    a = cast<T>(b + cube(b));
    for (T v : a) REQUIRE_THAT(v, WithinAbs(START + START * START * START, 1e-4));
  }
}

TEST_CASE("Binary Expressions") {
  const float scalar = GENERATE(-1.f, 0.f, 1.f, 3.f, 43.3f);

  Array<T, SIZE> a, b, c, d;
  fillWithRandom(a);
  fillWithRandom(b);

  c.clear();
  d.clear();

  SECTION("check: a + b") {
    for (int i = 0; i < a.size(); i++) c[i] = a[i] + b[i];
    d = a + b;

    for (int i = 0; i < c.size(); i++) REQUIRE_THAT(c[i], WithinAbs(d[i], 1e-4));
  }

  SECTION("check: a + scalar") {
    for (int i = 0; i < a.size(); i++) c[i] = a[i] + scalar;
    d = a + scalar;
    for (int i = 0; i < c.size(); i++) REQUIRE_THAT(c[i], WithinAbs(d[i], 1e-4));

    for (int i = 0; i < a.size(); i++) c[i] = scalar + a[i];
    d = scalar + a;
    for (int i = 0; i < c.size(); i++) REQUIRE_THAT(c[i], WithinAbs(d[i], 1e-4));
  }

  SECTION("check: a - b") {
    for (int i = 0; i < a.size(); i++) c[i] = a[i] - b[i];
    d = a - b;

    for (int i = 0; i < c.size(); i++) REQUIRE_THAT(c[i], WithinAbs(d[i], 1e-4));
  }

  SECTION("check: a - scalar") {
    for (int i = 0; i < a.size(); i++) c[i] = a[i] - scalar;
    d = a - scalar;
    for (int i = 0; i < c.size(); i++) REQUIRE_THAT(c[i], WithinAbs(d[i], 1e-4));

    for (int i = 0; i < a.size(); i++) c[i] = scalar - a[i];
    d = scalar - a;
    for (int i = 0; i < c.size(); i++) REQUIRE_THAT(c[i], WithinAbs(d[i], 1e-4));
  }

  SECTION("check: a * b") {
    for (int i = 0; i < a.size(); i++) c[i] = a[i] * b[i];
    d = a * b;

    for (int i = 0; i < c.size(); i++) REQUIRE_THAT(c[i], WithinAbs(d[i], 1e-4));
  }

  SECTION("check: a * scalar") {
    for (int i = 0; i < a.size(); i++) c[i] = a[i] * scalar;
    d = a * scalar;
    for (int i = 0; i < c.size(); i++) REQUIRE_THAT(c[i], WithinAbs(d[i], 1e-4));

    for (int i = 0; i < a.size(); i++) c[i] = scalar * a[i];
    d = scalar * a;
    for (int i = 0; i < c.size(); i++) REQUIRE_THAT(c[i], WithinAbs(d[i], 1e-4));
  }

  SECTION("check: a / b") {
    for (int i = 0; i < a.size(); i++) c[i] = a[i] / b[i];
    d = a / b;

    for (int i = 0; i < c.size(); i++) {
      REQUIRE_THAT(c[i], WithinAbs(d[i], 1e-4) || IsNaN());
      REQUIRE_THAT(d[i], WithinAbs(c[i], 1e-4) || IsNaN());
    }
  }

  SECTION("check: a / scalar") {
    for (int i = 0; i < a.size(); i++) c[i] = a[i] / scalar;
    d = a / scalar;
    for (int i = 0; i < c.size(); i++) {
      REQUIRE_THAT(c[i], WithinAbs(d[i], 1e-4) || IsNaN());
      REQUIRE_THAT(d[i], WithinAbs(c[i], 1e-4) || IsNaN());
    }

    for (int i = 0; i < a.size(); i++) c[i] = scalar / a[i];
    d = scalar / a;
    for (int i = 0; i < c.size(); i++) {
      REQUIRE_THAT(c[i], WithinAbs(d[i], 1e-4) || IsNaN());
      REQUIRE_THAT(d[i], WithinAbs(c[i], 1e-4) || IsNaN());
    }
  }

  SECTION("check: min(a, b)") {
    for (int i = 0; i < a.size(); i++) c[i] = std::min(a[i], b[i]);
    d = min(a, b);

    for (int i = 0; i < c.size(); i++)  REQUIRE_THAT(c[i], WithinAbs(d[i], 1e-4));
  }

  SECTION("check: min(a, scalar)") {
    for (int i = 0; i < a.size(); i++) c[i] = std::min(a[i], scalar);
    d = min(a, scalar);
    for (int i = 0; i < c.size(); i++)  REQUIRE_THAT(c[i], WithinAbs(d[i], 1e-4));

    for (int i = 0; i < a.size(); i++) c[i] = std::min(scalar, a[i]);
    d = min(scalar, a);
    for (int i = 0; i < c.size(); i++)  REQUIRE_THAT(c[i], WithinAbs(d[i], 1e-4));
  }

  SECTION("check: max(a, b)") {
    for (int i = 0; i < a.size(); i++) c[i] = std::max(a[i], b[i]);
    d = max(a, b);

    for (int i = 0; i < c.size(); i++)  REQUIRE_THAT(c[i], WithinAbs(d[i], 1e-4));
  }

  SECTION("check: max(a, scalar)") {
    for (int i = 0; i < a.size(); i++) c[i] = std::max(a[i], scalar);
    d = max(a, scalar);
    for (int i = 0; i < c.size(); i++)  REQUIRE_THAT(c[i], WithinAbs(d[i], 1e-4));

    for (int i = 0; i < a.size(); i++) c[i] = std::max(scalar, a[i]);
    d = max(scalar, a);
    for (int i = 0; i < c.size(); i++)  REQUIRE_THAT(c[i], WithinAbs(d[i], 1e-4));
  }
}

static float ifloat_step(float goal, int idx, int time) { return float(idx) * goal / float(time + 1); }

TEST_CASE("IFloat Expressions") {
  const auto goals = GENERATE(1.f, 2.f, 3.f, 42.33333f);
  const float GOAL = float(goals);

  Array<float, SIZE> a;
  a.clear();

  int i = 0;
  IFloat ifloat(0.f);

  SECTION("a = ifloat") {
    a = ifloat.set(GOAL, SIZE);
    for (float v : a) REQUIRE_THAT(v, WithinAbs(ifloat_step(GOAL, ++i, SIZE), 1e-4));
  }

  SECTION("a = b + ifloat") {
    Array<float, SIZE> b;
    fillWithRandom(b);

    a = b + ifloat.set(GOAL, SIZE);
    for (float v : a) {
      int j = i;
      REQUIRE_THAT(v, WithinAbs((b[j] + ifloat_step(GOAL, ++i, SIZE)), 1e-4));
    }
  }
}

TEST_CASE("Lambda expressions") {
  Array<T, SIZE> a;
  a.clear();

  Array<T, SIZE> b;
  fillWithRandom(b);

  SECTION("a = fun(a, cos)") {
    a = fun(b, cosf);
    for (int i = 0; i < a.size(); i++) REQUIRE_THAT(a[i], WithinAbs(cosf(b[i]), 1e-4));
  }

  SECTION("a = lambda(a, cos)") {
    a = lambda(b, cosf);
    for (int i = 0; i < a.size(); i++) REQUIRE_THAT(a[i], WithinAbs(cosf(b[i]), 1e-4));
  }

  SECTION("a = fun(a, v * v + 0.5)") {
    a = fun(b, [](float v) { return v * v + 0.5f; });
    for (int i = 0; i < a.size(); i++)
      REQUIRE_THAT(a[i], WithinAbs((b[i] * b[i] + 0.5f), 1e-4));
  }

  SECTION("a = lambda(a, v * v + 0.5)") {
    a = lambda(b, [](float v) { return v * v + 0.5f; });
    for (int i = 0; i < a.size(); i++)
      REQUIRE_THAT(a[i], WithinAbs((b[i] * b[i] + 0.5f), 1e-4));
  }

  SECTION("a = lambda(a, v * v * tmp) (tmp is captured)") {
    float tmp = 0.25f;
    a = lambda(b, [tmp](float v) { return v * v * tmp; });
    for (int i = 0; i < a.size(); i++)
      REQUIRE_THAT(a[i], WithinAbs((b[i] * b[i] * tmp), 1e-4));
  }
}

TEMPLATE_TEST_CASE("IFloat assignment (Arrays)", "", (Array<float, SIZE>)) {
  TestType a;
  IFloat ifloat(2.f);
  a = ifloat;
  for (T v : a) REQUIRE_THAT(v, WithinAbs(2.f, 1e-4));
}

TEMPLATE_TEST_CASE("IFloat assignment (Vectors)", "", (Vector<float>)) {
  TestType a(SIZE);
  IFloat ifloat(2.f);
  a = ifloat;
  for (T v : a) REQUIRE_THAT(v, WithinAbs(2.f, 1e-4));
}

TEMPLATE_TEST_CASE("Delayed assignment", "", (Array<T, SIZE>)) {
  const int delay = GENERATE(0, 1, 5, SIZE - 1, SIZE);

  Array<T, SIZE> a;
  Vector<T> b(SIZE);

  TestType out;

  fillWithRandom(a);
  fillWithRandom(b);
  out.clear();

  SECTION("delayAssign Array") {
    out.delayAssign(delay, a);
    for (int i = delay; i < SIZE; ++i)
      REQUIRE_THAT(out[i], WithinAbs(a[i - delay], 1e-4));
  }

  SECTION("delayAssign Vector") {
    out.delayAssign(delay, b);
    for (int i = delay; i < SIZE; ++i)
      REQUIRE_THAT(out[i], WithinAbs(b[i - delay], 1e-4));
  }
}

TEST_CASE("Containers Assignment") {
  Array<T, SIZE> in;
  fillWithRandom(in);

  SECTION("Array = Array") {
    Array<T, SIZE> a;
    a = in;
    for (int i = 0; i < in.size(); i++) REQUIRE_THAT(in[i], WithinAbs(a[i], 1e-4));

    fillWithRandom(a);
    in = a;
    for (int i = 0; i < in.size(); i++) REQUIRE_THAT(in[i], WithinAbs(a[i], 1e-4));
  }

  SECTION("Vector = Array") {
    Vector<T> a(SIZE);
    a = in;
    for (int i = 0; i < in.size(); i++) REQUIRE_THAT(in[i], WithinAbs(a[i], 1e-4));

    fillWithRandom(a);
    in = a;
    for (int i = 0; i < in.size(); i++) REQUIRE_THAT(in[i], WithinAbs(a[i], 1e-4));
  }
}

TEMPLATE_TEST_CASE("Benchmark ScalarExpr", "[!benchmark][expr]", (Array<T, SIZE>),
                   (Array<T, SIZE << 6>)) {
  TestType a;
  fillWithRandom(a);

  BENCHMARK("for: a = 0.5_f") {
    for (T &v : a) v = 0.5f;
    return &a;
  };

  BENCHMARK("expr: a = 0.5_f") { return &(a = 0.5f); };

  BENCHMARK("for: a += 0.5_f") {
    for (T &v : a) v += 0.5f;
    return &a;
  };

  BENCHMARK("expr: a += 0.5_f") { return &(a += 0.5f); };

  BENCHMARK("for: a -= 0.5_f") {
    for (T &v : a) v -= 0.5f;
    return &a;
  };

  BENCHMARK("expr: a -= 0.5_f") { return &(a -= 0.5f); };

  BENCHMARK("for: a *= 0.5_f") {
    for (T &v : a) v *= 0.5f;
    return &a;
  };

  BENCHMARK("expr: a *= 0.5_f") { return &(a *= 0.5f); };

  BENCHMARK("for: a /= 0.5_f") {
    for (T &v : a) v /= 0.5f;
    return &a;
  };

  BENCHMARK("expr: a /= 0.5_f") { return &(a /= 0.5f); };
}

TEMPLATE_TEST_CASE("Benchmark Lambdas", "[!benchmark][expr]", (Array<T, SIZE>),
                   (Array<T, SIZE << 6>)) {
  TestType a;
  TestType b;

  a.clear();
  fillWithRandom(b);

  BENCHMARK("for loop cos") {
    for (int i = 0; i < a.size(); i++) a[i] = cosf(b[i]);
    return &a;
  };

  BENCHMARK("fun cos") { return &(a = fun(b, cosf)); };

  BENCHMARK("lambda cos") { return &(a = lambda(b, cosf)); };
}

TEST_CASE("Benchmark forward", "[!benchmark][expr]") {
  const int size = GENERATE(SIZE, SIZE << 6);
  const int fw = GENERATE(0.f, 0.25f, 0.5f) * size;
  const int forwardSize = size + fw;

  CAPTURE(size, fw);

  Vector<T> a(forwardSize);
  Vector<T> b(forwardSize);
  Vector<T> out(size);

  fillWithRandom(a);
  fillWithRandom(b);
  out.clear();

  std::string for_loop_title = "for loop (forward = " + std::to_string(fw) + ")";
  std::string expression_title = "expression (forward = " + std::to_string(fw) + ")";

  BENCHMARK(for_loop_title.data()) {
    for (int i = fw; i != forwardSize; ++i) out[i - fw] = a[i] + b[i];
    return &out;
  };

  BENCHMARK(expression_title.data()) { return &(out = forward(a + b, fw)); };
}

TEMPLATE_TEST_CASE("Benchmark Unary Expressions", "[!benchmark][expr]", (Array<T, SIZE>),
                   (Array<T, SIZE << 6>)) {
  TestType a;
  TestType b;

  fillWithRandom(a);
  b.clear();

  BENCHMARK("for: abs(a)") {
    for (int i = 0; i < a.size(); i++) b[i] = std::abs(a[i]);
    return &b;
  };

  BENCHMARK("expr: abs(a)") { return &(b = abs(a)); };

  BENCHMARK("for: square(a)") {
    for (int i = 0; i < a.size(); i++) b[i] = a[i] * a[i];
    return &b;
  };

  BENCHMARK("expr: square(a)") { return &(b = square(a)); };

  BENCHMARK("for: cube(a)") {
    for (int i = 0; i < a.size(); i++) b[i] = a[i] * a[i] * a[i];
    return &b;
  };

  BENCHMARK("expr: cube(a)") { return &(b = cube(a)); };

  BENCHMARK("for: -a") {
    for (int i = 0; i < a.size(); i++) b[i] = -a[i];
    return &b;
  };

  BENCHMARK("expr: -a") { return &(b = -a); };

  BENCHMARK("for: a++") {
    for (int i = 0; i < a.size(); i++) b[i] = a[i]++;
    return &b;
  };

  BENCHMARK("expr: a++") { return &(b = a++); };

  BENCHMARK("for: a--") {
    for (int i = 0; i < a.size(); i++) b[i] = a[i]--;
    return &b;
  };

  BENCHMARK("expr: a--") { return &(b = a--); };
}

TEMPLATE_TEST_CASE("Benchmark Binary Expressions", "[!benchmark][expr]", (Array<T, SIZE>),
                   (Array<T, SIZE << 6>)) {
  TestType a;
  TestType b;

  fillWithRandom(a);
  b.clear();

  BENCHMARK("for: a + b") {
    for (int i = 0; i < a.size(); i++) b[i] = a[i] + a[i];
    return &b;
  };

  BENCHMARK("expr: a + b") { return &(b = a + a); };

  BENCHMARK("for: a - b") {
    for (int i = 0; i < a.size(); i++) b[i] = a[i] - a[i];
    return &b;
  };

  BENCHMARK("expr: a - b") { return &(b = a - a); };

  BENCHMARK("for: a * b") {
    for (int i = 0; i < a.size(); i++) b[i] = a[i] * a[i];
    return &b;
  };

  BENCHMARK("expr: a * b") { return &(b = a * a); };

  BENCHMARK("for: a / b") {
    for (int i = 0; i < a.size(); i++) b[i] = a[i] / a[i];
    return &b;
  };

  BENCHMARK("expr: a / b") { return &(b = a / a); };

  BENCHMARK("for: min(a, b)") {
    for (int i = 0; i < a.size(); i++) b[i] = std::min(a[i], 5.f);
    return &b;
  };

  BENCHMARK("expr: min(a, b)") { return &(b = min(a, 5.f)); };

  BENCHMARK("for: max(a, b)") {
    for (int i = 0; i < a.size(); i++) b[i] = std::max(a[i], 5.f);
    return &b;
  };

  BENCHMARK("expr: max(a, b)") { return &(b = max(a, 5.f)); };
}

TEMPLATE_TEST_CASE("Benchmark complex expressions", "[!benchmark][complex_expr]", (Array<T, SIZE>),
                   (Array<T, SIZE << 6>)) {
  TestType a;
  TestType b;
  TestType c;
  TestType d;

  fillWithRandom(a);
  fillWithRandom(b);
  fillWithRandom(c);

  BENCHMARK("for loop TINY") {
    for (int i = 0; i < a.size(); i++) d[i] = a[i] + b[i];
    return &d;
  };

  BENCHMARK("expr template TINY") { return &(d = a + b); };

  BENCHMARK("for loop MEDIUM") {
    for (int i = 0; i < a.size(); i++) {
      T v1 = (b[i] * a[i]);
      T v2 = a[i] + (v1 * v1) / c[i];
      d[i] = v2 * v2 * v2 - b[i];
    }
    return &d;
  };

  BENCHMARK("expr template MEDIUM") { return &(d = cube(a + square(b * a) / c) - b); };

  BENCHMARK("for loop BIG") {
    for (int i = 0; i < a.size(); i++) {
      T v1 = a[i] - b[i];
      T v2 = v1 * v1 * c[i];

      T v3 = b[i] * a[i];
      T v4 = a[i] + v3 * v3 / c[i];

      d[i] = 0.5f * std::min(v2 * v2 * v2 + a[i], v4 * v4 * v4 - b[i]);
    }
    return &d;
  };

  BENCHMARK("expr template BIG") {
    return &(d = 0.5f * min(cube(square(a - b) * c) + a, cube(a + square(b * a) / c) - b));
  };
}