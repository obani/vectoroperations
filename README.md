# VectorOperations


This is a test project to experiment expression templates applied to vector operations.

**Required:**
- cmake
- Catch2

### Build: the easy way

`make debug` or `make release` to build with optimizations.
This will launch the test program automatically.

Default is `release`

### Build: the annoying way

**Debug mode:**
```
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Debug ..
cmake --build .
```

**With optimizations:**
```
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
cmake --build .
```


# EXPLANATION

Expression templates are structures that permit to do operations on vectors like you would do with cell by cell for loops operations, but writing them in a simpler way.

### Add vector types

If you want it to be possible for some buffer/array/vector types to be assigned an expression, you need to extend your structure following the template below:

```cpp

struct MyVectorType : ExprAssign<MyVectorType> {
    using MyVectorType::operator=; // Permits to use the assignement operator from ExprAssign which takes expressions as parameters.
};
```

Some Array and Vector classes are available in `expressions.h`

### Example

```cpp
#include <iostream>
#include <math.h>
#include "expression_templates.h"

// This example should cover every basic expression you can do

int main() {
    static constexpr SIZE = 32;
    Array<double, SIZE> a;
    Array<double, SIZE> b;
    Array<double, SIZE> out;

    // Scalar expression
    out = 1.0; // Converts a scalar to an expression

    // Binary expressions
    out = a + b;
    out = a - b;
    out = a * b;
    out = a / b;
    out = min(a, b);
    out = max(a, b);

    // Unary expressions
    out = a++; // This is possible only if 'a' is a container ('Array' here) and not an expression
    out = a--; // This is possible only if 'a' is a container ('Array' here) and not an expression
    out = -a;
    out = abs(a);
    out = square(a);
    out = cube(a);

    // Forward expression
    constexpr int forwardSkip = 4;
    Array<double, SIZE + forwardSkip> toForward;

    // Skip the 'forwardSkip' first values of 'd', 'd' needs to be at least the size of 'c' plus 'forwardSkip'
    out = forward(toForward, forwardSkip); // forwarSkip must be an integer

    // Cast expression
    Array<int, SIZE> toCast;
    out = cast<double>(toCast); // Casts 'toCast' to type 'f' if a constructor exists

    // Lambda expressions

    // The only difference between 'fun' and 'lambda' is that 'fun' doesn't allow capturing parameters
    // This makes 'fun' a more restrictive option than 'lambda', but also a more efficient one because it only relies on function pointers 

    double pi = 3.14159 * 2.0; 
    out = fun(a, cos); // Apply function 'std::cos' to 'a'
    out = fun(a, [](f v) { return cos(v); }); // Apply a lambda with no capture to 'a'
    out = lambda(a, [pi](f v) { return cos(v * pi); }); // Apply any function to 'a'

    // Unless specified that you can't, every expression stated earlier can take expressions as arguments.
    // For example:
    out = min(0.5, fun(cube((a + b) * b), cos));

    // Expressions can be stored and iterated on
    // For example:

    for(f v : a + b) std::cout << v << std::endl; // Prints the results of a + b for each cell

    auto e = a + b;
    for(f v : e) std::cout << v << std::endl; // Prints the results of a + b for each cell

    auto e2 = e * a;
    for(f v : e2) std::cout << v << std::endl; // Prints the results of (a + b) * a for each cell

    // Operators '=', '+=', '-=', '*=', '/=', pre '++' and pre '--' instantly process the results to the buffer
    // For example:
    out = e;
    out += e;
    out -= e;
    out *= e;
    out /= e;
    ++out;
    --out;

    // Expressions are anything that contains an input iterator and return a size
    // For example:
    IFloat ifloat(0.f);
    out = a + ifloat.set(10.f, SIZE);
}
```

# GRAMMAR

```
T (unknown type)
I (int)

A (arithmetic)
F (function)

C (container) :=
  ++C | --C | C = E |
  C += E | C -= E |
  C *= E | C /= E

E (expression) :=
  E + E | E + A | A + E |
  E - E | E - A | A - E |
  E * E | E * A | A * E | 
  E / E | E / A | A / E |
  min(E, E) | min(E, A) | min(A, E) |
  max(E, E) | max(E, A) | max(A, E) |
  abs(E) | square(E) | cube(E) |
  -E |
  cast<T>(E) |        // cast an expression E of type T2 to type T
  forward(E, I) |     // skip the I first values of E
  fun(E, F) |         // apply function F to E (function with no capture)
  lambda(E, F) |      // apply function F to E (function with capture, prefer fun if you can)
  ++E |
  C++ | C-- | C
```