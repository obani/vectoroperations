#include <cassert>
#include <cstdint>
#include <vector>

enum UnOp {
  PostIncrement,
  PostDecrement,
  Abs,
  Square,
  Cube,
  Negate,
};
enum BinOp { Add, Sub, Mul, Div, Min, Max };

//////////////////////////////////////////////////
/*                                              */
/*                                              */
/*                  CONCEPTS                    */
/*                                              */
/*                                              */
//////////////////////////////////////////////////

template <typename T>
concept arithmetic = std::is_arithmetic_v<T> || requires(T &v) { v.repr(); };

// An expression has a value_type, a size, and can generate an iterator
template <class E>
concept expression = requires(E &e) {
  typename E::value_type;
  e.size();
  e.begin();
  e.end();
};

// A leaf is an expression with a defined iterator type
template <class E>
concept leaf = requires(E &e) { typename E::iterator; } && expression<E>;

template <class E, typename T>
concept expressionWithType = expression<E> && std::is_same_v<typename E::value_type, T>;

template <class E>
concept expressionOrArithmetic = expression<E> || arithmetic<E>;

// A container is a structure you can acces with operator[] to a reference of an object
template <class C>
concept container = requires(C &c) { c[0] = typename C::value_type {}; } && leaf<C>;

template <typename T>
concept hasAbs = requires(T &t) { t.abs(); };

template <typename T>
concept hasMin = requires(T &t) { t.min(t); };

template <typename T>
concept hasMax = requires(T &t) { t.max(t); };

//////////////////////////////////////////////////
/*                                              */
/*                                              */
/*                 EXPRESSIONS                  */
/*                                              */
/*                                              */
//////////////////////////////////////////////////

template <expression E>
struct Leaf {
  using value_type = typename E::value_type;
  using iterator = typename E::iterator;
  using return_type = std::conditional_t<container<E>, value_type &, value_type>;

  constexpr Leaf(const E &e) : data_(const_cast<E &>(e)), it_(data_.begin()) {}
  constexpr int size() const { return data_.size(); }
  iterator &operator++() { return ++it_; }
  return_type operator*() { return *it_; }

 private:
  E &data_;
  iterator it_;
};

template <typename E, typename T>
struct Rexpr {
 private:
  struct Iterator {
    Iterator(E &e, int p) : e_(e), pos_(p) {}
    Iterator &operator++() {
      ++pos_;
      ++e_;
      return *this;
    }
    bool operator!=(const Iterator &it) { return pos_ != it.pos_; }
    typename E::value_type operator*() { return *e_; }

   private:
    E &e_;
    int pos_;
  };

  E &operator++() { return static_cast<E &>(*this).next(); }
  T operator*() { return static_cast<E &>(*this).curr(); }

 public:
  friend struct ExprCopy;
  template <arithmetic T_>
  friend struct ScalarExpr;
  template <expression E_, UnOp op_>
  friend struct UnaryExpr;
  template <expression E1_, expressionWithType<typename E1_::value_type> E2_, BinOp op_>
  friend struct BinaryExpr;
  template <expression E_>
  friend struct ForwardExpr;
  template <expression E_, arithmetic T_>
  friend struct CastExpr;
  template <expression E_, typename F_>
  friend struct LambdaExpr;

  Iterator begin() { return Iterator(*((E *)this), 0); }
  Iterator end() { return Iterator(*((E *)this), ((E *)this)->size()); }
};

template <arithmetic T>
struct ScalarExpr : Rexpr<ScalarExpr<T>, T> {
  template <typename E_, typename T_>
  friend struct Rexpr;
  using value_type = T;

  constexpr ScalarExpr(const value_type &x) : x_(x) {}
  constexpr int size() const { return -1; }

 private:
  constexpr ScalarExpr &next() { return *this; }
  constexpr value_type curr() { return x_; }

  const T x_;
};

template <expression E, UnOp op>
struct UnaryExpr : Rexpr<UnaryExpr<E, op>, typename E::value_type> {
  template <typename E_, typename T_>
  friend struct Rexpr;
  using value_type = typename E::value_type;

  constexpr UnaryExpr(const E &a) : a_(a) {}
  constexpr int size() const { return a_.size(); }

 private:
  constexpr UnaryExpr &next() {
    ++a_;
    return *this;
  }

  constexpr value_type curr() {
    if constexpr (op == PostIncrement && container<E>) return (*a_)++;
    else if constexpr (op == PostDecrement && container<E>) return (*a_)--;
    else if constexpr (op == Abs && hasAbs<value_type>) return (*a_).abs();
    else if constexpr (op == Abs) return std::abs(*a_);
    else if constexpr (op == Negate) return -(*a_);
    else {
      value_type v = *a_;
      if constexpr (op == Square) return v * v;
      else if constexpr (op == Cube) return v * v * v;
    }
  }

  std::conditional_t<leaf<E>, Leaf<E>, E> a_;
};

template <expression E1, expressionWithType<typename E1::value_type> E2, BinOp op>
struct BinaryExpr : Rexpr<BinaryExpr<E1, E2, op>, typename E1::value_type> {
  template <typename E_, typename T_>
  friend struct Rexpr;
  using value_type = typename E1::value_type;

  constexpr BinaryExpr(const E1 &a, const E2 &b) : a_(a), b_(b) {
    assert(a.size() == b.size() || a.size() == -1 || b.size() == -1);
  }
  constexpr int size() const { return std::max(a_.size(), b_.size()); }

 private:
  constexpr BinaryExpr &next() {
    ++a_;
    ++b_;
    return *this;
  }

  constexpr value_type curr() {
    if constexpr (op == Add) return *a_ + *b_;
    else if constexpr (op == Sub) return *a_ - *b_;
    else if constexpr (op == Mul) return *a_ * *b_;
    else if constexpr (op == Div) return *a_ / *b_;
    else if constexpr (op == Min && hasMin<value_type>) return (*a_).min(*b_);
    else if constexpr (op == Min) return std::min(*a_, *b_);
    else if constexpr (op == Max && hasMax<value_type>) return (*a_).max(*b_);
    else if constexpr (op == Max) return std::max(*a_, *b_);
  }

  std::conditional_t<leaf<E1>, Leaf<E1>, E1> a_;
  std::conditional_t<leaf<E2>, Leaf<E2>, E2> b_;
};

template <expression E>
struct ForwardExpr : Rexpr<ForwardExpr<E>, typename E::value_type> {
  template <typename E_, typename T_>
  friend struct Rexpr;
  using value_type = typename E::value_type;

  constexpr ForwardExpr(const E &a, unsigned int steps) : a_(a) {
    for (unsigned int i = 0; i != steps; ++i) ++a_;
  }
  constexpr int size() const { return -1; }

 private:
  constexpr ForwardExpr &next() {
    ++a_;
    return *this;
  }

  constexpr value_type curr() { return *a_; }

  std::conditional_t<leaf<E>, Leaf<E>, E> a_;
};

template <expression E, arithmetic T>
struct CastExpr : Rexpr<CastExpr<E, T>, T> {
  template <typename E_, typename T_>
  friend struct Rexpr;
  using value_type = T;

  constexpr CastExpr(const E &a) : a_(a) {}
  constexpr int size() const { return a_.size(); }

 private:
  constexpr CastExpr &next() {
    ++a_;
    return *this;
  }

  constexpr T curr() { return T(*a_); }

  std::conditional_t<leaf<E>, Leaf<E>, E> a_;
};

template <expression E, typename F>
struct LambdaExpr : Rexpr<LambdaExpr<E, F>, typename E::value_type> {
  template <typename E_, typename T_>
  friend struct Rexpr;
  using value_type = typename E::value_type;

  constexpr LambdaExpr(const E &a, const F &f) : a_(a), fun_(f) {}
  constexpr int size() const { return a_.size(); }

 private:
  constexpr LambdaExpr &next() {
    ++a_;
    return *this;
  }

  constexpr value_type curr() { return fun_(*a_); }

  std::conditional_t<leaf<E>, Leaf<E>, E> a_;
  F fun_;
};

//////////////////////////////////////////////////
/*                                              */
/*                                              */
/*                   BUFFERS                    */
/*                                              */
/*                                              */
//////////////////////////////////////////////////

struct ExprCopy {
  template <class Base>
  friend struct ExprAssign;

  template <container C, expressionOrArithmetic E>
  friend constexpr C &operator+=(C &c, const E &e);
  template <container C, expressionOrArithmetic E>
  friend constexpr C &operator-=(C &c, const E &e);
  template <container C, expressionOrArithmetic E>
  friend constexpr C &operator*=(C &c, const E &e);
  template <container C, expressionOrArithmetic E>
  friend constexpr C &operator/=(C &c, const E &e);
  template <container C>
  friend constexpr C &operator++(C &c);
  template <container C>
  friend constexpr C &operator--(C &c);

 private:
  template <container C, expressionWithType<typename C::value_type> E>
  static constexpr void assignExpr(C &c, const E &e, typename C::iterator itContainer) {
    auto itEnd = c.end();
    auto itExpression = const_cast<E &>(e).begin();
    for (; itContainer != itEnd; ++itContainer, ++itExpression) *itContainer = *itExpression;
  }

  template <container C, expressionWithType<typename C::value_type> E>
  static constexpr C &copyExpr(C &c, const E &e) {
    assignExpr(c, e, c.begin());
    return c;
  }

  template <container C, expressionWithType<typename C::value_type> E>
  static constexpr C &copyExprDelayed(C &c, const E &e, unsigned int delay) {
    auto itContainer = c.begin();
    for (unsigned int i = 0; i != delay; ++i) ++itContainer;
    assignExpr(c, e, itContainer);
    return c;
  }
};

template <class Base>
struct ExprAssign {
  template <expression E>
  constexpr Base &operator=(const E &e) {
    return ExprCopy::copyExpr(*((Base *)this), e);
  }

  template <arithmetic T>
  constexpr Base &operator=(T v) {
    return ExprCopy::copyExpr(*((Base *)this), ScalarExpr(v));
  }

  template <expression E>
  constexpr Base &delayAssign(unsigned int delay, const E &e) {
    return ExprCopy::copyExprDelayed(*((Base *)this), e, delay);
  }

  template <arithmetic T>
  constexpr Base &delayAssign(unsigned int delay, T v) {
    return ExprCopy::copyExprDelayed(*((Base *)this), ScalarExpr(v), delay);
  }
};

struct IFloat {
private:
  struct Iterator {
    Iterator(IFloat &f) : f_(f) {}
    Iterator &operator++() { return *this; }
    bool operator!=(const Iterator &) { return true; }
    float operator*() { return f_.next(); }

  private:
    IFloat &f_;
  };

public:
  using value_type = float;
  using iterator = Iterator;

  explicit IFloat(float value) noexcept : value_(value), increment_(0.f) {}
  IFloat() noexcept : value_(0.f), increment_(0.f) {}

  float next() noexcept { return value_ += increment_; }

  IFloat &set(float value, int time) noexcept {
    increment_ = (value - value_) * (1.f / float(time + 1));
    return *this;
  }

  // Instantly sets the value
  void jump(float value) noexcept {
    value_ = value;
    increment_ = 0.f;
  }

  // returns the current value
  float getValue() const { return value_; }
  int size() const { return -1; }

  Iterator begin() { return Iterator(*this); }
  Iterator end() { return Iterator(*this); }

private:
  float value_, increment_;
};

template <typename T, int SIZE> struct Array : ExprAssign<Array<T, SIZE>> {
  using value_type = T;
  using iterator = T *;
  using ExprAssign<Array<T, SIZE>>::operator=;

  constexpr int size() const { return SIZE; }
  constexpr T *data() { return data_; };
  constexpr const T *data() const { return data_; };
  constexpr T &operator[](size_t at) { return data_[at]; }
  constexpr const T &operator[](size_t at) const { return data_[at]; }

  T *begin() { return data_; }
  const T *cbegin() const { return data_; }
  T *end() { return data_ + SIZE; }
  const T *cend() const { return data_ + SIZE; }

  constexpr void clear() {
    for (T &v : *this)
      v = 0;
  }

  friend std::ostream &operator<<(std::ostream &os, Array<T, SIZE> &vec) {
    for (const T &v : vec)
      os << v << " ";
    return os;
  }

private:
  T data_[SIZE];
};

template <typename T>
struct Vector : public std::vector<T>, ExprAssign<Vector<T>> {
  using value_type = T;
  using ExprAssign<Vector<T>>::operator=;

  Vector(std::initializer_list<T> list) : std::vector<T>(list) {}
  Vector(int s) : std::vector<T>((size_t)s) {}

  constexpr int size() const { return (int)std::vector<T>::size(); }

  friend std::ostream &operator<<(std::ostream &os, const Vector<T> &vec) {
    for (T v : vec)
      os << v << " ";
    return os;
  }
};

//////////////////////////////////////////////////
/*                                              */
/*                                              */
/*                  OPERATORS                   */
/*                                              */
/*                                              */
//////////////////////////////////////////////////

// operator+

template <expression E1, expression E2>
constexpr BinaryExpr<E1, E2, BinOp::Add> operator+(const E1 &lhs, const E2 &rhs) {
  return BinaryExpr<E1, E2, BinOp::Add>(lhs, rhs);
}

template <expression E, typename T>
constexpr BinaryExpr<E, ScalarExpr<T>, BinOp::Add> operator+(const E &lhs, T rhs) {
  return BinaryExpr<E, ScalarExpr<T>, BinOp::Add>(lhs, ScalarExpr(rhs));
}

template <expression E, typename T>
constexpr BinaryExpr<ScalarExpr<T>, E, BinOp::Add> operator+(T lhs, const E &rhs) {
  return BinaryExpr<ScalarExpr<T>, E, BinOp::Add>(ScalarExpr(lhs), rhs);
}

// operator-

template <expression E1, expression E2>
constexpr BinaryExpr<E1, E2, BinOp::Sub> operator-(const E1 &lhs, const E2 &rhs) {
  return BinaryExpr<E1, E2, BinOp::Sub>(lhs, rhs);
}

template <expression E, typename T>
constexpr BinaryExpr<E, ScalarExpr<T>, BinOp::Sub> operator-(const E &lhs, T rhs) {
  return BinaryExpr<E, ScalarExpr<T>, BinOp::Sub>(lhs, ScalarExpr(rhs));
}

template <expression E, typename T>
constexpr BinaryExpr<ScalarExpr<T>, E, BinOp::Sub> operator-(T lhs, const E &rhs) {
  return BinaryExpr<ScalarExpr<T>, E, BinOp::Sub>(ScalarExpr(lhs), rhs);
}

// operator*

template <expression E1, expression E2>
constexpr BinaryExpr<E1, E2, BinOp::Mul> operator*(const E1 &lhs, const E2 &rhs) {
  return BinaryExpr<E1, E2, BinOp::Mul>(lhs, rhs);
}

template <expression E, typename T>
constexpr BinaryExpr<E, ScalarExpr<T>, BinOp::Mul> operator*(const E &lhs, T rhs) {
  return BinaryExpr<E, ScalarExpr<T>, BinOp::Mul>(lhs, ScalarExpr(rhs));
}

template <expression E, typename T>
constexpr BinaryExpr<ScalarExpr<T>, E, BinOp::Mul> operator*(T lhs, const E &rhs) {
  return BinaryExpr<ScalarExpr<T>, E, BinOp::Mul>(ScalarExpr(lhs), rhs);
}

// operator/

template <expression E1, expression E2>
constexpr BinaryExpr<E1, E2, BinOp::Div> operator/(const E1 &lhs, const E2 &rhs) {
  return BinaryExpr<E1, E2, BinOp::Div>(lhs, rhs);
}

template <expression E, typename T>
constexpr BinaryExpr<E, ScalarExpr<T>, BinOp::Div> operator/(const E &lhs, T rhs) {
  return BinaryExpr<E, ScalarExpr<T>, BinOp::Div>(lhs, ScalarExpr(rhs));
}

template <expression E, typename T>
constexpr BinaryExpr<ScalarExpr<T>, E, BinOp::Div> operator/(T lhs, const E &rhs) {
  return BinaryExpr<ScalarExpr<T>, E, BinOp::Div>(ScalarExpr(lhs), rhs);
}

// min

template <expression E1, expression E2>
constexpr BinaryExpr<E1, E2, BinOp::Min> min(const E1 &lhs, const E2 &rhs) {
  return BinaryExpr<E1, E2, BinOp::Min>(lhs, rhs);
}

template <expression E, typename T>
constexpr BinaryExpr<E, ScalarExpr<T>, BinOp::Min> min(const E &lhs, T rhs) {
  return BinaryExpr<E, ScalarExpr<T>, BinOp::Min>(lhs, ScalarExpr(rhs));
}

template <expression E, typename T>
constexpr BinaryExpr<ScalarExpr<T>, E, BinOp::Min> min(T lhs, const E &rhs) {
  return BinaryExpr<ScalarExpr<T>, E, BinOp::Min>(ScalarExpr(lhs), rhs);
}

// max

template <expression E1, expression E2>
constexpr BinaryExpr<E1, E2, BinOp::Max> max(const E1 &lhs, const E2 &rhs) {
  return BinaryExpr<E1, E2, BinOp::Max>(lhs, rhs);
}

template <expression E, typename T>
constexpr BinaryExpr<E, ScalarExpr<T>, BinOp::Max> max(const E &lhs, T rhs) {
  return BinaryExpr<E, ScalarExpr<T>, BinOp::Max>(lhs, ScalarExpr(rhs));
}

template <expression E, typename T>
constexpr BinaryExpr<ScalarExpr<T>, E, BinOp::Max> max(T lhs, const E &rhs) {
  return BinaryExpr<ScalarExpr<T>, E, BinOp::Max>(ScalarExpr(lhs), rhs);
}

// a++

template <container C>
constexpr UnaryExpr<C, UnOp::PostIncrement> operator++(const C &c, int) {
  return UnaryExpr<C, UnOp::PostIncrement>(c);
}

// a--

template <container C>
constexpr UnaryExpr<C, UnOp::PostDecrement> operator--(const C &c, int) {
  return UnaryExpr<C, UnOp::PostDecrement>(c);
}

// abs

template <expression E>
constexpr UnaryExpr<E, UnOp::Abs> abs(const E &expr) {
  return UnaryExpr<E, UnOp::Abs>(expr);
}

// square

template <expression E>
constexpr UnaryExpr<E, UnOp::Square> square(const E &expr) {
  return UnaryExpr<E, UnOp::Square>(expr);
}

// cube

template <expression E>
constexpr UnaryExpr<E, UnOp::Cube> cube(const E &expr) {
  return UnaryExpr<E, UnOp::Cube>(expr);
}

// unary operator-

template <expression E>
constexpr UnaryExpr<E, UnOp::Negate> operator-(const E &expr) {
  return UnaryExpr<E, UnOp::Negate>(expr);
}

// forward

template <expression E>
constexpr ForwardExpr<E> forward(const E &expr, unsigned int steps) {
  return ForwardExpr<E>(expr, steps);
}

// cast

template <arithmetic T, expression E>
constexpr CastExpr<E, T> cast(const E &e) {
  return CastExpr<E, T>(e);
}

// fun (no capture)
// uses function pointers (no capture allowed)

template <expression E>
constexpr LambdaExpr<E, typename E::value_type (*)(typename E::value_type)> fun(
    const E &e, typename E::value_type (*f)(typename E::value_type)) {
  return LambdaExpr<E, typename E::value_type (*)(typename E::value_type)>(e, f);
}

// lambda
// uses std::function (capture allowed)

template <expression E>
constexpr LambdaExpr<E, std::function<typename E::value_type(typename E::value_type)>> lambda(
    const E &e, const std::function<typename E::value_type(typename E::value_type)> &f) {
  return LambdaExpr<E, std::function<typename E::value_type(typename E::value_type)>>(e, f);
}

// operator+=

template <container C, expressionOrArithmetic E>
constexpr C &operator+=(C &c, const E &e) {
  return ExprCopy::copyExpr(c, operator+(c, e));
}

// operator-=

template <container C, expressionOrArithmetic E>
constexpr C &operator-=(C &c, const E &e) {
  return ExprCopy::copyExpr(c, operator-(c, e));
}

// operator*=

template <container C, expressionOrArithmetic E>
constexpr C &operator*=(C &c, const E &e) {
  return ExprCopy::copyExpr(c, operator*(c, e));
}

// operator/=

template <container C, expressionOrArithmetic E>
constexpr C &operator/=(C &c, const E &e) {
  return ExprCopy::copyExpr(c, operator/(c, e));
}

// ++a

template <container C>
constexpr C &operator++(C &c) {
  return ExprCopy::copyExpr(c, operator+(c, typename C::value_type(1)));
}

// --a

template <container C>
constexpr C &operator--(C &c) {
  return ExprCopy::copyExpr(c, operator-(c, typename C::value_type(1)));
}
